extern crate factorial;
use factorial::Factorial;
use rocket_contrib::json::Json;

#[derive(Serialize, Deserialize)]
pub struct Binomial {
    p: f32, 
    n: u32, 
    x: u32
}

impl Binomial {
    fn combination(n: u32, x: u32) -> u32 {
        n.factorial()/((n-x).factorial()*(x).factorial())
    }

    fn first_multiplication(p: f32, x: u32) -> f32 {
        f32::powf(p, x as f32)
    }

    fn second_multiply(p: f32, n: u32, x: u32) -> f32 {
        f32::powf(1.0-p, (n-x) as f32)
    }

    pub fn get_result(binomials: Json<Binomial>) -> f32 {
        let combination = Binomial::combination(binomials.n, binomials.x) as f32;
        combination*Binomial::first_multiplication(binomials.p, binomials.x)*Binomial::second_multiply(binomials.p, binomials.n, binomials.x)
    }
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_combination_ten_and_five() {
        assert_eq!(Binomial::combination(10, 5), 252)
    }

    #[test]
    fn test_combination_seven_and_four() {
        assert_eq!(Binomial::combination(7, 4), 35)
    }

    #[test]
    fn test_combination_twelve_and_three() {
        assert_eq!(Binomial::combination(12, 3), 220)
    }

    #[test]
    fn test_combination_five_and_three() {
        assert_eq!(Binomial::combination(5, 3), 10)
    }

    #[test]
    fn test_first_multiplication() {
        assert_eq!(Binomial::first_multiplication(0.05, 3), 0.000125)
    }

    #[test]
    fn test_second_multiply() {
        assert_eq!(Binomial::second_multiply(0.05, 5, 3), 0.9025)
    }

}
