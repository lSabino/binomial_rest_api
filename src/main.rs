#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;

extern crate math;

mod binomial;
use rocket::Rocket;
use rocket_contrib::json::JsonValue;
use rocket_contrib::json::Json;
use rocket::http::Method;
use rocket_cors::{AllowedOrigins, AllowedHeaders};
use binomial::Binomial;
use math::round;


#[post("/get_result", data="<parameter>")]
fn get_result_web(parameter: Json<Binomial>) -> JsonValue {
    json!(
        round::floor((Binomial::get_result(parameter)*100.0) as f64, 3)
    )
}

fn rocket_partial() -> Rocket {
    rocket::ignite().mount("/v1/", routes![get_result_web])
}

fn main() {
    rocket_partial().launch();
}


#[cfg(test)]
mod test {
    use crate::*;
    use super::rocket;
    use rocket::local::Client;
    use rocket::http::Status;
    use rocket::http::ContentType;


    #[test]
    fn get_result_web_test() {
        let client = Client::new(rocket_partial()).expect("valid rocket instance");
        let mut response = client.post("/v1/get_result").body("{\"p\": 0.05, \"n\": 5, \"x\": 2}").dispatch();
        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.content_type(), Some(ContentType::JSON));
        assert_eq!(response.body_string(), Some("2.143".into()));
    }
}